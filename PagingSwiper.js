import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View
} from 'react-native';

import Swiper from 'react-native-swiper';

const styles = StyleSheet.create({
  wrapper: {
  },
  slide1: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  slide2: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  slide3: {
    flex: 2,
    justifyContent: 'center',
    alignItems: 'center',
  },
  descriptionText: {
    fontSize:14,
    color:'#808080',
    fontFamily: 'Cochin',
  }
})

export default class PagingSwiper extends Component {
  render(){
    return (
      <Swiper style={styles.wrapper} 
              showsButtons={false}
              activeDot={<View style={{backgroundColor: "#4E30EF", width: 8, height: 8, borderRadius: 7, marginLeft: 7, marginRight: 7}} />}
              index={3}
      >
     
        <View style={styles.slide2}>

        </View>
        <View style={styles.slide3}>
            <View>
                <Text style={styles.descriptionText}>
                          Never a better time than now to start 
                </Text>
            </View>
            <View style={{paddingTop:10}}>
                <Text style={styles.descriptionText}>
                          think about how you manage all your
                </Text>
            </View>
            <View style={{paddingTop:10}}>
                <Text style={styles.descriptionText}>
                      finances with ease.
                </Text>
            </View>
        </View>
      </Swiper>
    );
  }
}
