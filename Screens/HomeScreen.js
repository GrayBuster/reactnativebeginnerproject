'use strict';
import React, { Component } from 'react';
import { 
  StyleSheet,
  Text, 
  View,
  Image,
  ScrollView,
  Button,
  TouchableOpacity,
  NavigatorIOS
} from 'react-native';
import PropTypes from 'prop-types';
//import PagingSwiper from './PagingSwiper';

export default class HomeScreen extends Component<{}> {

  constructor(props) {
        super(props);
        
        this.state = { 
            one: false,
            two: false,
            three: true,
        };
  }

  static navigationOptions = {
    header: null,
    headerStyle: {
        borderBottomWidth: 0,
        backgroundColor:'#F7F6FB'    
    },
    headerTitleStyle: {
        fontSize:15,
        color:'#9B97AF',
    },
    headerTintColor: 'black',
  }

  _toRegistration() {
      var step = 1;
      this.props.navigation.navigate('Registration', {
              step: step,
            });
  }

  _toLogin() {

  }
  
  _onPressedDot(number) {
      switch (number) {
          case 1:
            this.setState({ 
                one: true ,
                two:false,
                three:false,
   
            });
            break;
           case 2:
            this.setState({ 
                one: false ,
                two:true,
                three:false,

            });
            break;
            case 3:
            this.setState({ 
                one: false ,
                two:false,
                three:true,

            });
            break;
            default:
                break;
      }
  }

  render() {
    return (
      <View style={{flex:1}}>
          {/*Image & text view container*/}
          <View style={styles.imgContainer}>
            <Image style={styles.homePageImage}
                source={require('../Images/HomePageImg.jpg')}
            />
            <Text style={{fontSize:22,fontWeight:'bold',paddingTop:20,paddingBottom:20,fontFamily: 'Cochin'}}>
                Let's get started
            </Text>
            {/*Scroll text view*/}
            <View style={styles.scrollViewContainer}>
              <ScrollView 
                  pagingEnabled={true}
                  horizontal 
                  showsHorizontalScrollIndicator={false} 
                  scrollEventThrottle={10}>
                  <View style={{flex:1,alignItems: 'center'}}>
                    <View>
                      <Text style={styles.descriptionText}>
                          Never a better time than now to start 
                      </Text>
                    </View>
                    <View style={{paddingTop:10}}>
                      <Text style={styles.descriptionText}>
                          think about how you manage all your
                      </Text>
                    </View>
                    <View style={{paddingTop:10}}>
                      <Text style={styles.descriptionText}>
                        finances with ease.
                      </Text>
                    </View>
                  </View>
              </ScrollView>
              {/*Dot view container*/}
              <View style={styles.dotContainer}>
                <TouchableOpacity onPress={() => this._onPressedDot(1)}>
                    <View style={this.state.one ? styles.activeDot : styles.dot}>
                    </View>
                </TouchableOpacity>

                <TouchableOpacity onPress={() => this._onPressedDot(2)}>
                    <View style={this.state.two ? styles.activeDot : styles.dot}>

                    </View>
                </TouchableOpacity>
                  
                <TouchableOpacity onPress={() => this._onPressedDot(3)}>
                    <View style={this.state.three ? styles.activeDot : styles.dot}>

                    </View>
                </TouchableOpacity>
                  
              </View>
                {/*Button view container*/}
                <View style={styles.buttonContainer}>
                    <TouchableOpacity onPress={(this._toRegistration.bind(this))}>
                      <View style={styles.createBtn}>
                          <Text style={styles.createText}>
                            Create Account
                          </Text>
                      </View>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={this._toLogin.bind(this)}>
                      <View style={styles.loginBtn}>
                          <Text style={styles.loginText}>
                            Login
                          </Text>
                      </View>
                    </TouchableOpacity>
                </View>
            </View>
          </View>
      </View>
    );
  }
}




const styles = StyleSheet.create({
  imgContainer: {
    flex:1,
    alignItems: 'center',
    padding:70,
    backgroundColor: '#F7F6FB',
  },

  homePageImage: {
    width:197,
    height:184,
  },
  descriptionText: {
    fontSize:15,
    color:"#808080",
    fontFamily: 'Cochin',
  },
  activeDot: {
    backgroundColor:'#4E30EF',
    width:7,
    height:7,
    borderRadius: 25,
    marginLeft: 10,
  },
  dot: {
    backgroundColor:'#D7D6DB',
    width:7,
    height:7,
    borderRadius: 25,
    marginLeft: 10,
  },
  dotContainer: {
    flex:1,
    flexDirection: 'row',
    justifyContent:'center',
    marginRight: 15,
    marginBottom: 80
  },
  buttonContainer: {
      flex:1,
      paddingBottom: 30
  },
  scrollViewContainer: {
    flex:1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  loginBtn: {
    backgroundColor:"#FEFEFE",
    width:300,
    height:40,
    borderRadius:20,
    marginTop:20,
    alignItems:'center',
    justifyContent:'center',
    borderWidth: 2,
    borderColor:'#EDEDED'
  },
  createBtn: {
    backgroundColor:"#7A70FE",
    width:300,
    height:40,
    borderRadius:20,
    alignItems:'center',
    justifyContent:'center'
  },
  loginText: {
    fontSize:16,
    color:'black',
    fontFamily: 'Cochin',
    fontWeight:'100'
  },
  createText: {
    fontSize:16,
    color:'white',
    fontFamily: 'Cochin',
    fontWeight:'100'
  }
});