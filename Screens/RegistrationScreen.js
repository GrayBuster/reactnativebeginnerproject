'use strict';
import React, { Component } from 'react';
import { 
  StyleSheet,
  Text, 
  View,
  Image,
  ScrollView,
  Button,
  TouchableOpacity,
  NavigatorIOS,
  TextInput
} from 'react-native';
import FontAwesome, { Icons } from 'react-native-fontawesome';
import { Ionicons } from '@expo/vector-icons';
import { Font } from 'expo';
import { KeyboardAvoidingView } from 'react-native';
import { Header } from 'react-navigation';
import CustomTitleNavigation from '../Screens/CustomTitleNavigation'


export default class RegistrationScreen extends React.Component {
  
    state = {
        fontLoaded: false,
    };
    async componentDidMount() {
        await Font.loadAsync({
        'Font Awesome': require('../assets/fonts/fontawesome.ttf'),
        });
    }
    _toNextStep() {
        var step = this.props.navigation.state.params.step + 1
        this.props.navigation.navigate('StepTwo', {
            step:step
        });
    }

    onChanged(text) {
        let newText = '';
        let numbers = '0123456789';

        for (var i=0; i < text.length; i++) {
            if(numbers.indexOf(text[i]) > -1 ) {
                newText = newText + text[i];
            }
            else {
                
                alert("please enter numbers only");
            }
        }
        this.setState({ myNumber: newText });
    }

    static navigationOptions = ({ navigation }) => ({
        headerTitle: <CustomTitleNavigation step={navigation.state.params.step}/>,
        headerStyle: {
            backgroundColor: '#F7F6FB',
            borderBottomWidth: 0,
        },
        headerTintColor: 'black',
        
    });
  render() {
      
    return (
      <KeyboardAvoidingView style={styles.container}
                            keyboardVerticalOffset = {Header.HEIGHT - 60 } 
                            behavior="position" enabled
                            >
        <View style={styles.imgContainer}>
            {/* Image */}
            <Image style={styles.stepOneImage}
                source={require('../Images/StepOneImage.jpg')}
            />
            {/* Regis Text */}
            <Text style={{fontSize:22,fontWeight:'300',paddingTop:20,paddingBottom:20,fontFamily: 'Kailasa'}}>
                Registration
            </Text>
            {/* Description text */}
            <View style={{flex:1,alignItems: 'center'}}>
                    <View>
                      <Text style={styles.descriptionText}>
                          Enter your mobile number,we will send you
                      </Text>
                    </View>
                    <View style={{paddingTop:10}}>
                      <Text style={styles.descriptionText}>
                          OTP to verify later
                      </Text>
                    </View>
                    {/* Phone input container */}
                    <View style={styles.verifiedContainer}>
                        <View style={styles.btnContainer}>
                            <View style={styles.phoneInputContainer}>
                                
                                <View style={{marginRight:5}}>
                                    <Image 
                                        source={require('../Images/Flag.jpg')}
                                    />
                                </View>
                                <View style={{marginRight:5}}>
                                    <Text>
                                        (+62)
                                    </Text>
                                </View>
        
                                <TextInput 
                                    style={styles.phoneTextInput}
                                    //keyboardType='numeric'
                                    onChangeText={(text)=> this.onChanged(text)}
                                    value={'8128008011'}
                                    maxLength={10}
                                    placeholder="Phone number"
                                />
                                 <View style={{flexDirection: 'row',marginLeft:35}}>
                                    {
                                        this.state.fontLoaded ? (
                                            <Text style={styles.checkIcon}>
                                                
                                            </Text>
                                        ) : null
                                    }
                                </View>
                                

                            </View>
                            <View style={styles.continueContainer}>
                                <TouchableOpacity onPress={this._toNextStep.bind(this)}>
                                    <View style={styles.continueBtn}>
                                        <Text style={styles.continueText}>
                                            Continue
                                        </Text>
                                    </View>
                                </TouchableOpacity>
                            </View>
                            
                        </View>
                        
                    </View>
            </View>
            
        </View>
        

      </KeyboardAvoidingView>
    );
  }
}


const styles = StyleSheet.create({
  container: {
    flex:1,
    backgroundColor: '#F7F6FB',
    justifyContent: 'center',
    alignItems:'center'
  },
  title: {
      fontSize:15,
      color:'#9B97AF',
      paddingRight: 3
  },
  titleContainer: {
      flex:1,
      flexDirection:'row',
      justifyContent:'center',
  },
  imgContainer: {
    flex:1,
    alignItems: 'center',
    padding:20,
    backgroundColor: '#F7F6FB',
  },
  stepOneImage: {
    width:107,
    height:94,
  },
  descriptionText: {
    fontSize:13,
    color:"#808080",
    fontFamily: 'Kailasa',
    fontWeight:'200'
  },
  continueBtn: {
    backgroundColor:"#7A70FE",
    width:250,
    height:40,
    borderRadius:20,
    alignItems:'center',
    justifyContent:'center',
    marginTop: 10
  },
  continueText: {
    fontSize:16,
    color:'white',
    fontFamily: 'Cochin',
    fontWeight:'100'
  },
  verifiedContainer: {
    width: 300,
    height: 150,
    alignItems: 'center',
    justifyContent:'center',
    backgroundColor: 'white',
    marginTop: 40
  },
  btnContainer: {
      width: 150,
      height: 90,
      alignItems: 'center',
      justifyContent:'center',
  },
  phoneInputContainer: {
      flex: 1,
      flexDirection:'row',
      borderRadius: 2,
      borderWidth: 2,
      borderColor: '#EDEDED',
      width:250,
      height:80,
      alignItems:'center',
      justifyContent:'center',
  },
  continueContainer: {
      flex: 1,
      marginTop: 10,
      marginBottom: 10  
  },
  phoneTextInput: {
      height: 75,
      width: 100,
  },
  checkIcon: {
      color:'#24A454',
      fontFamily: 'Font Awesome', 
      fontSize: 20,
      fontWeight:'normal',
      alignSelf: 'flex-end', 
      
  }
});