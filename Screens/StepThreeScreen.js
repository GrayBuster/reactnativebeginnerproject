'use strict';
import React, { Component } from 'react';
import { 
  StyleSheet,
  Text, 
  View,
  Image,
  ScrollView,
  Button,
  TouchableOpacity,
  NavigatorIOS,
  TextInput
} from 'react-native';
import FontAwesome, { Icons } from 'react-native-fontawesome';
import { Ionicons } from '@expo/vector-icons';
import { Font } from 'expo';
import CustomTitleNavigation from '../Screens/CustomTitleNavigation'


export default class StepThreeScreen extends React.Component {
  constructor(props) {
        super(props);
        
    
  }

  static navigationOptions = ({ navigation }) => ({
        headerTitle: <CustomTitleNavigation step={navigation.state.params.step}/>,
        headerTintColor: 'black',
        headerStyle: {
            backgroundColor: '#F7F6FB',
            borderBottomWidth: 0,
        },
        headerTitleStyle: {
            fontSize:15,
            color:'#9B97AF',

        },
    });
    _toNextStep() {
        var step = this.props.navigation.state.params.step + 1
        this.props.navigation.navigate('StepFour', {
            step:step
        });
    }
    render() {
      
    return (
      <View style={styles.container}>
          <View style={styles.imgContainer}>
               <Image style={styles.stepThreeImage}
                  source={require('../Images/StepThreeImage.jpg')}
               />
            {/* Fingerprint Text */}
            <Text style={{fontSize:22,fontWeight:'300',paddingTop:20,paddingBottom:20,fontFamily: 'Kailasa'}}>
                Fingerprint
            </Text>
            {/* Description text */}
            <View style={{flex:1,alignItems: 'center'}}>
                    <View>
                      <Text style={styles.descriptionText}>
                          To add your fingerprint lift and rest your
                      </Text>
                    </View>
                    <View style={{paddingTop:10}}>
                      <Text style={styles.descriptionText}>
                          finger at home button repeatedly (optional)
                      </Text>
                    </View>
                    {/* Fingerprint container */}
                    <View style={styles.fingerprintContainer}>
                          <Image style={styles.fingerImg}
                                source={require('../Images/Fingerprint.jpg')}
                          />
                         
                          
                          
                          <View style={styles.btnContainer}>
                                {/* Skip btn */}
                                <TouchableOpacity onPress={this._toNextStep.bind(this)}>
                                        <View style={styles.skipBtn}>
                                            <Text style={styles.skipText}>
                                                Skip this step 
                                            </Text>
                                        </View>
                                </TouchableOpacity>
                                {/* Continue btn */}
                                <TouchableOpacity onPress={this._toNextStep.bind(this)}>
                                    <View style={styles.continueBtn}>
                                        <Text style={styles.continueText}>
                                        Continue
                                        </Text>
                                    </View>
                                </TouchableOpacity>
                          </View>        
                    </View>
            </View>
            
        </View>
      </View>
    );
  }
}


const styles = StyleSheet.create({
  container: {
    flex:1,
    backgroundColor: '#F7F6FB',
    justifyContent: 'center'
  },
  title: {
      fontSize:15,
      color:'#9B97AF',
      paddingRight: 3
  },
  titleContainer: {
      flex:1,
      flexDirection:'row',
      justifyContent:'center',
  },
  imgContainer: {
    flex:1,
    alignItems: 'center',
    padding:20,
    backgroundColor: '#F7F6FB',
  },
  stepThreeImage: {
    width:147,
    height:134,
  },
  fingerImg: {
    // width:147,
    height:154,
    marginTop:20
  },
  descriptionText: {
    fontSize:13,
    color:"#808080",
    fontFamily: 'Kailasa',
    fontWeight:'200'
  },
  continueBtn: {
    backgroundColor:"#7A70FE",
    width:250,
    height:40,
    borderRadius:20,
    alignItems:'center',
    justifyContent:'center',
    marginTop: 5
  },
  continueText: {
    fontSize:16,
    color:'white',
    fontFamily: 'Cochin',
    fontWeight:'100'
  },
  skipBtn: {
    backgroundColor:"#F7F6FB",
    width:250,
    height:40,
    alignItems:'center',
    justifyContent:'center',
    marginBottom: 10
  },
  skipText: {
      fontSize:13,
      color:'#7A70FE',
      fontFamily: 'Font Awesome',
      fontWeight:'normal'
  },
  fingerprintContainer: {
    flex:1,
    alignItems: 'center',
    justifyContent:'center',
    marginTop: 20
  },
  imgFingerContainer: {
      flex: 1,
      alignItems:'center',
      justifyContent:'center',
      marginLeft: 20
  },
  btnContainer: {
      flex: 1,
      justifyContent:'flex-end',
      marginTop: 50,
      backgroundColor: '#F7F6FB',  
  },
});