'use strict';
import React, { Component } from 'react';
import { 
  StyleSheet,
  Text, 
  View,
  Image,
  ScrollView,
  Button,
  TouchableOpacity,
  NavigatorIOS,
  TextInput
} from 'react-native';
import FontAwesome, { Icons } from 'react-native-fontawesome';
import { Font } from 'expo';
import Triangle from 'react-native-triangle';
import FinalStepScreen from './FinalStepScreen';
import Modal from 'react-native-modalbox';
import CustomTitleNavigation from '../Screens/CustomTitleNavigation'


export default class StepFourScreen extends React.Component {
  constructor(props) {
        super(props);
        
    
  }

  

  static navigationOptions = ({ navigation }) => ({
        headerTitle: <CustomTitleNavigation step={navigation.state.params.step}/>,
        headerTintColor: 'black',
        headerStyle: {
            backgroundColor: '#F7F6FB',
            borderBottomWidth: 0,
        },
        headerTitleStyle: {
            fontSize:15,
            color:'#9B97AF',

        },
    });

    _toNextStep() {
        this.refs.centerModal.open();
    
        
         //finalStepScreen._showScreen();
    }
    render() {
      
    return (
      <View style={styles.container}>
          {/* Popup final step */}
          <Modal
            style={[styles.modal,styles.centerModal]}
            position={"center"}
            ref={"centerModal"}
          >
            <FinalStepScreen/>
          </Modal>  
          <View style={styles.imgContainer}>
               <Image style={styles.stepThreeImage}
                  source={require('../Images/StepFourImage.jpg')}
               />
            {/* Title Text */}
            <Text style={{fontSize:20,fontWeight:'bold',paddingTop:30,paddingBottom:10,fontFamily: 'Kailasa',textAlign:'center'}}>
                One step away to your {'\n'} account
            </Text>
            {/* Description text */}
            <View style={{flex:1}}>
                <View style={{justifyContent:'center',alignItems:'center'}}>
                    <Text style={styles.descriptionText}>
                        Now we need to verify your identity
                    </Text>
                </View>
                {/* Fingerprint container */}
                <View style={styles.fingerprintContainer}>
                          
                    {/* Method one container */}     
                    <View style={styles.methodContainer}>

                        <View style={styles.identityCardContainer}>
                            <View style={styles.stepOneView}>
                                <Text style={{color:'white'}}>
                                    1
                                </Text>    
                            </View>
                            <View style={{flex:1,marginRight:10,marginTop:12}}>
                                <Text style={{fontWeight:'bold',fontSize:16}}>
                                    Use identify card
                                </Text>    
                            </View>
                               
                            <View style={styles.recommendedContainer}>
                                <View style={styles.triangle}></View> 
                                <Text style={{textAlign:'center',fontSize:10,color:'white'}}>
                                        Recommended
                                </Text>
                            </View>
                        </View>
                        <View style={styles.methodTextContainer}>
                            <Text style={styles.descriptionText}>
                                Get the full feature of your IndoAlliz! {'\n'}
                                You can store up to IDR 10 Million
                                
                            </Text>
                        </View>
                    </View>
                    {/* Method two container */}
                    <View style={styles.methodContainer}>

                        <View style={styles.identityCardContainer}>
                            <View style={styles.stepOneView}>
                                <Text style={{color:'white'}}>
                                    2
                                </Text>    
                            </View>
                            <View style={{flex:1,marginRight:10,marginTop:12}}>
                                <Text style={{fontWeight:'bold',fontSize:16}}>
                                    Without identity card
                                </Text>    
                            </View>
                        </View>
                        <View style={styles.methodTextContainer}>
                            <Text style={styles.descriptionText}>
                                You only can store IDR 1 Million to {'\n'}
                                your IndoAlliz wallet
                            </Text>
                        </View>
                    </View>
                          
                        <View style={styles.btnContainer}>
                            {/* Continue btn */}
                            <TouchableOpacity onPress={this._toNextStep.bind(this)}>
                                <View style={styles.continueBtn}>
                                    <Text style={styles.continueText}>
                                        Continue
                                    </Text>
                                </View>
                            </TouchableOpacity>
                          </View>        
                    </View>
            </View>
            
        </View>
      </View>
    );
  }
}


const styles = StyleSheet.create({
  container: {
    flex:1,
    backgroundColor: '#F7F6FB',
    justifyContent: 'center'
  },
  title: {
      fontSize:15,
      color:'#9B97AF',
      paddingRight: 3
  },
  titleContainer: {
      flex:1,
      flexDirection:'row',
      justifyContent:'center',
  },
  imgContainer: {
    flex:1,
    alignItems: 'center',
    padding:20,
    backgroundColor: '#F7F6FB',
  },
  stepThreeImage: {
    width:147,
    height:134,
    backgroundColor: '#F7F6FB',
  },
  descriptionText: {
    fontSize:14,
    color:"#808080",
    fontFamily: 'Cochin',
    fontWeight:'200',
    //textAlign:'center'
    textAlign: 'left', 
    //alignSelf: 'stretch'
  },
  continueBtn: {
    backgroundColor:"#7A70FE",
    width:250,
    height:40,
    borderRadius:20,
    alignItems:'center',
    justifyContent:'center',
    marginTop: 5,
    marginBottom: 10
  },
  continueText: {
    fontSize:16,
    color:'white',
    fontFamily: 'Cochin',
    fontWeight:'100'
  },
  methodContainer: {
      width:300,
      height: 100,
      borderRadius: 5,
      backgroundColor:'white',
      marginBottom: 20
  },
  identityCardContainer: {
      flex:1,
      flexDirection:'row',
  },
  fingerprintContainer: {
    flex:1,
    alignItems: 'center',
    justifyContent:'center',
    marginTop: 20
  },
  imgFingerContainer: {
      flex: 1,
      alignItems:'center',
      justifyContent:'center',
      marginLeft: 20
  },
  btnContainer: {
      flex: 1,
      justifyContent:'flex-end',
      marginTop: 50,
      backgroundColor: '#F7F6FB',  
  },
  stepOneView: {
      borderRadius: 25,
      backgroundColor: '#22D15D',
      width: 25,
      height: 25,
      marginRight: 10,
      justifyContent: 'center',
      alignItems: 'center',
      marginLeft: 10,
      marginTop: 10
  },
  recommendedContainer: {
      backgroundColor: '#7A70FE',
      width: 80,
      height: 25,
      marginTop: 10,
      justifyContent: 'center',
      borderTopLeftRadius: 3,
      borderBottomLeftRadius:3,
      borderRightWidth:1,
      borderTopWidth: 1,
      borderColor:'#7A70FE'
  },
  methodTextContainer: {
        justifyContent:'center',
       alignItems:'center',
      marginBottom:20
  },
  triangle: {
    position: 'absolute',
    left: -11,
    borderTopColor: 'transparent',
    borderTopWidth: 13,
    borderRightWidth: 13,
    borderRightColor: '#7A70FE',
    borderBottomWidth: 13,
    borderBottomColor: 'transparent'
  },
  modal: {
    justifyContent: 'flex-end',
    alignItems: 'center'
  },
  centerModal: {
    height: 370,
    width: 300,
    borderRadius: 5
  },
});