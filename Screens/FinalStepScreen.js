'use strict';
import React, { Component } from 'react';
import { 
  StyleSheet,
  Text, 
  View,
  Image,
  ScrollView,
  Button,
  TouchableOpacity,
  NavigatorIOS,
  TextInput
} from 'react-native';
import FontAwesome, { Icons } from 'react-native-fontawesome';
import { Font } from 'expo';
import Modal from 'react-native-modalbox';

export default class FinalStepScreen extends React.Component {
    componentDidMount() {
        
    }

    _showScreen(){
        //alert(this.refs.centerModal)
    }

    render() {
    return (
      
      <View style={styles.container}>
          <View style={styles.imgContainer}>
               <Image style={styles.finalStepImage}
                  source={require('../Images/FinalStepImage.jpg')}
               />
            
            
            
            
            <View style={{justifyContent:'center',alignItems:'center',marginTop:20}}>
                {/* Congratulation text */}
                <Text style={styles.descriptionText}>
                    Congratulation
                </Text>
                {/* Title Text */}
                <Text style={{fontSize:22,fontWeight:'bold',paddingTop:7,paddingBottom:7,fontFamily: 'Cochin'}}>
                    Now you are registered
                </Text>
                {/* Description text */}
                <Text style={styles.descriptionText}>
                    Get ready with IndoAlliz wallet
                </Text>
            </View>
            
            <View style={{flex:1,alignItems: 'center',marginBottom:10}}>
                    {/* Successful container */}
                    <View style={styles.successfulContainer}>            
                          <View style={styles.btnContainer}>
                                {/* Start btn */}
                                <TouchableOpacity onPress={() => {}}>
                                    <View style={styles.startBtn}>
                                        <Text style={styles.startText}>
                                            Start Now
                                        </Text>
                                    </View>
                                </TouchableOpacity>
                          </View>        
                    </View>
                </View>
                
            </View>
      </View>
      
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex:1,
    backgroundColor: '#F7F6FB',
    justifyContent: 'center'
  },
  title: {
      fontSize:15,
      color:'#9B97AF',
      paddingRight: 3
  },
  titleContainer: {
      flex:1,
      flexDirection:'row',
      justifyContent:'center',
  },
  imgContainer: {
    flex:1,
    alignItems: 'center',
    padding:20,
    backgroundColor: '#F7F6FB',
  },
  finalStepImage: {
    width:147,
    height:134,
  },
  descriptionText: {
    fontSize:13,
    color:"#808080",
    fontFamily: 'Cochin',
    fontWeight:'600'
  },
  startBtn: {
    backgroundColor:"#7A70FE",
    width:250,
    height:40,
    borderRadius:20,
    alignItems:'center',
    justifyContent:'center',
    marginTop: 5
  },
  startText: {
    fontSize:16,
    color:'white',
    fontFamily: 'Cochin',
    fontWeight:'100'
  },
  successfulContainer: {
    flex:1,
    alignItems: 'center',
    justifyContent:'center',
  },
  btnContainer: {
      flex: 1,
      justifyContent:'flex-end',
      marginTop: 50,
      backgroundColor: '#F7F6FB',  
  },
  
});