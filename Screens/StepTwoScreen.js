'use strict';
import React, { Component } from 'react';
import { 
  StyleSheet,
  Text, 
  View,
  Image,
  ScrollView,
  Button,
  TouchableOpacity,
  NavigatorIOS,
  TextInput
} from 'react-native';
import FontAwesome, { Icons } from 'react-native-fontawesome';
import { Ionicons } from '@expo/vector-icons';
import { Font } from 'expo';
import { KeyboardAvoidingView } from 'react-native';
import { Header } from 'react-navigation';
import CustomTitleNavigation from '../Screens/CustomTitleNavigation'



export default class StepTwoScreen extends React.Component {
  constructor(props) {
        super(props);
        
        this.state = { 
            textOne: false,
            textTwo: false,
            textThree: false,
            textFour: false,
        };
  }
  _onPressed(number) {
      switch (number) {
          case 1:
            this.setState({ 
                textOne: true ,
                textTwo:false,
                textThree:false,
                textFour:false,
            });
            break;
           case 2:
            this.setState({ 
                textOne: false ,
                textTwo:true,
                textThree:false,
                textFour:false,
            });
            break;
            case 3:
            this.setState({ 
                textOne: false ,
                textTwo:false,
                textThree:true,
                textFour:false,
            });
            break;
            case 4:
            this.setState({ 
                textOne: false ,
                textTwo:false,
                textThree:false,
                textFour:true,
            });
            break;
            default:
                break;
      }
        
    }
  _onResign() {
        this.setState({ 
                textOne: false ,
                textTwo:false,
                textThree:false,
                textFour:false,
            });
  }  
  static navigationOptions = ({ navigation }) => ({
        headerTitle: <CustomTitleNavigation step={navigation.state.params.step}/>,
        headerTintColor: 'black',
        headerStyle: {
            backgroundColor: '#F7F6FB',
            borderBottomWidth: 0,
        },
        headerTitleStyle: {
            fontSize:15,
            color:'#9B97AF',
        },
    });

    _toNextStep() {
        var step = this.props.navigation.state.params.step + 1
        this.props.navigation.navigate('StepThree', {
            step:step
        });
    }
    render() {
      
    return (
      <KeyboardAvoidingView style={styles.container}
                            keyboardVerticalOffset = {Header.HEIGHT - 60 } 
                            behavior="position" enabled
                            >
          <View style={styles.imgContainer}>
               <Image style={styles.stepTwoImage}
                  source={require('../Images/StepTwoImage.jpg')}
               />
            {/* Verify Text */}
            <Text style={{fontSize:22,fontWeight:'300',paddingTop:20,paddingBottom:20,fontFamily: 'Kailasa'}}>
                Verification
            </Text>
            {/* Description text */}
            <View style={{flex:1,alignItems: 'center'}}>
                    <View>
                      <Text style={styles.descriptionText}>
                          Enter 4 digit number that sent to
                      </Text>
                    </View>
                    <View style={{paddingTop:10}}>
                      <Text style={styles.descriptionText}>
                          +62900382349
                      </Text>
                    </View>
                    {/* Verified input container */}
                    <View style={styles.verifiedContainer}>
                      <View style={styles.btnContainer}>
                          <View style={styles.verifiedInputContainer}>
                            <View style={
                                    this.state.textOne ? styles.selectedSquareContainer : styles.squareContainer
                                    }>
                                <TextInput 
                                    style={styles.verifiedTextInput}
                                    onTouchStart={() =>  this._onPressed(1)}
                                    textAlign={'center'}
                                    onSubmitEditing={() => this._onResign()}
                                    value={'8'}
                                    maxLength={1}
                                />
                            </View>
                            <View style={this.state.textTwo ? styles.selectedSquareContainer : styles.squareContainer}>
                                <TextInput 
                                    style={styles.verifiedTextInput}
                                    textAlign={'center'}
                                    onTouchStart={() =>  this._onPressed(2)}
                                    onSubmitEditing={() => this._onResign()}
                                    value={'2'}
                                    maxLength={1}
                                />
                            </View>
                            <View style={this.state.textThree ? styles.selectedSquareContainer : styles.squareContainer}>
                                <TextInput 
                                    style={styles.verifiedTextInput}
                                    textAlign={'center'}
                                    onTouchStart={() =>  this._onPressed(3)}
                                
                                    onSubmitEditing={() => this._onResign()}
                                    value={'4'}
                                    maxLength={1}
                                />
                            </View>    
                            <View style={this.state.textFour ? styles.selectedSquareContainer : styles.squareContainer}>
                                <TextInput 
                                    style={styles.verifiedTextInput}
                                    textAlign={'center'}
                                    
                                    onTouchStart={() =>  this._onPressed(4)}
                                    onSubmitEditing={() => this._onResign()}
                                    
                                    maxLength={1}
                                />
                            </View>      

                          </View>
                          <View style={styles.continueContainer}>
                              <TouchableOpacity onPress={this._toNextStep.bind(this)}>
                                  <View style={styles.continueBtn}>
                                    <Text style={styles.continueText}>
                                      Continue
                                    </Text>
                                  </View>
                              </TouchableOpacity>
                          </View>        
                      </View>
                    </View>
            </View>
            
        </View>
      </KeyboardAvoidingView>
    );
  }
}


const styles = StyleSheet.create({
  container: {
    flex:1,
    backgroundColor: '#F7F6FB',
    justifyContent: 'center',
    alignItems:'center'
  },
  title: {
      fontSize:15,
      color:'#9B97AF',
      paddingRight: 3
  },
  titleContainer: {
      flex:1,
      flexDirection:'row',
      justifyContent:'center',
  },
  imgContainer: {
    flex:1,
    alignItems: 'center',
    padding:20,
    backgroundColor: '#F7F6FB',
  },
  stepTwoImage: {
    width:147,
    height:134,
  },
  descriptionText: {
    fontSize:13,
    color:"#808080",
    fontFamily: 'Kailasa',
    fontWeight:'200'
  },
  continueBtn: {
    backgroundColor:"#7A70FE",
    width:250,
    height:40,
    borderRadius:20,
    alignItems:'center',
    justifyContent:'center',
    marginTop: 10
  },
  continueText: {
    fontSize:16,
    color:'white',
    fontFamily: 'Cochin',
    fontWeight:'100'
  },
  verifiedContainer: {
    width: 300,
    height: 150,
    alignItems: 'center',
    justifyContent:'center',
    backgroundColor: 'white',
    marginTop: 40
  },
  btnContainer: {
      width: 150,
      height: 90,
      alignItems: 'center',
      justifyContent:'center',
  },
  verifiedInputContainer: {
      flex: 1,
      flexDirection:'row',
      width:250,
      height:80,
      alignItems:'center',
      justifyContent:'center',
      marginLeft: 20
  },
  continueContainer: {
      flex: 1,
      marginTop: 10,
      marginBottom: 10  
  },
  verifiedTextInput: {
      height: 40,
      width: 40,
  },
  checkIcon: {
      color:'#24A454',
      fontFamily: 'Font Awesome', 
      fontSize: 20,
      fontWeight:'normal',
      alignSelf: 'flex-end', 
      
  },
  squareContainer: {
    height: 40,
    width: 40,
    borderBottomEndRadius:3,
    borderBottomColor: '#F6F6F6',
    borderBottomWidth:1,
    alignItems:'center',
    justifyContent:'center',
    marginRight: 20
  },
  selectedSquareContainer: {
    height: 40,
    width: 40,
    marginRight: 20,
    borderBottomEndRadius:3,
    borderBottomColor: 'gray',
    borderBottomWidth:1,
    alignItems:'center',
    justifyContent:'center',
  }
});
