import React, { Component } from 'react';
import { 
  StyleSheet,
  Text, 
  View,
  Scene 
} from 'react-native';
import HomeScreen from '../Screens/HomeScreen';
import RegistrationScreen from '../Screens/RegistrationScreen';
import StepTwoScreen from '../Screens/StepTwoScreen';
import StepThreeScreen from '../Screens/StepThreeScreen';
import StepFourScreen from '../Screens/StepFourScreen';
import { createStackNavigator } from 'react-navigation';

export default class App extends React.Component {
  
  render() {
    return (
      <View style={styles.container}>

        {/* <HomePage style={{flex:1}}/> */}
        <AppStackNavigator />
      </View>
    );
  }
}

{/* Navigation */}
const AppStackNavigator = createStackNavigator({
  
  Home: HomeScreen,
  Registration: RegistrationScreen,
  StepTwo: StepTwoScreen,
  StepThree: StepThreeScreen,
  StepFour: StepFourScreen,
   
},
);

const styles = StyleSheet.create({
  container: {
    flex:1,
    backgroundColor: '#F7F6FB',
  },
  homePage: {
    justifyContent: 'center',
    alignItems: 'center',
    flex:1
  }
});
