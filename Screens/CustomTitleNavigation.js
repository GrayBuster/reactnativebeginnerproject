'use strict';
import React, { Component } from 'react';
import { 
  StyleSheet,
  Text, 
  View,
  Image,
  ScrollView,
  Button,
  TouchableOpacity,
  NavigatorIOS,
  TextInput
} from 'react-native';

export default class CustomTitleNavigation extends React.Component {
    render() {
        const { step } = this.props;
        
        return(
            <View style={styles.titleContainer}>
                <Text style={styles.title}>
                    Step 
                </Text>

                <Text style={{fontWeight:'100',fontFamily:'Bradley Hand',fontSize:17}}>
                    {step}
                   
                </Text>

                <Text style={styles.title}>
                    /5
                </Text>
            </View>
        );
    }
}


const styles = StyleSheet.create({
  title: {
      fontSize:15,
      color:'#9B97AF',
      paddingRight: 3
  },
  titleContainer: {
      flex:1,
      flexDirection:'row',
      justifyContent:'center',
  }
});